/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.patricknugent;

import com.dkit.gd2.Exceptions.DaoException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlDAO
{
    public Connection getConnection() throws DaoException
    {
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/OOPCA6";
        String username = "root";
        String password = "";
        Connection con = null;

        try
        {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("Failed to find driver class" + e.getMessage());
            System.exit(1);
        }
        catch(SQLException e)
        {
            System.out.println("Connection failed" + e.getMessage());
            System.exit(2);
        }
        System.out.println("Connected to database successfully");
        return con;
    }

    public void freeConnection(Connection con) throws DaoException
    {
        try
        {
            if(con != null)
            {
                con.close();
                con = null;
            }
        }
        catch(SQLException e)
        {
            System.out.println("Failed to free the connection" + e.getMessage());
            System.exit(1);
        }
    }
}