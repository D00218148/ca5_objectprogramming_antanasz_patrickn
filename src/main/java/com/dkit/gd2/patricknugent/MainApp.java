/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.patricknugent;
import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.*;
import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLOutput;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainApp
{
    static ITollEventDaoInterface ITollEventDao = new MySqlTollEventDAO();
    static Scanner keyboard = new Scanner(System.in);

    /*static
    {
        try
        {
            lookupTable = ITollEventDao.getAllRegistrations();
        }
        catch (DaoException de)
        {
            System.out.println(de.getMessage());
        }
    }*/

    private static Set<String> lookupTable = new HashSet<>();
    private static List<CustomerBill> customerBills = new ArrayList<>();
    private static List<String> invalidRegistrations = new ArrayList<>();
    private static Map<String, List<TollEvent>> tollEventsMap = new HashMap<>();
    private static List<TollEvent> tollEventsToRemove = new ArrayList<>();

    public enum MenuEnum
    {
        HEARTBEAT("heartbeat"), BILLING("billing"), WRITE("write"), VALID("valid"), INVALID("invalid"), EVENTDETAILS("event details"), REGISTRATIONDETAILS("registration details"), EVENTSSINCE("events since"), EVENTSBETWEEN("events between"), REGISTRATIONS("registrations"), EVENTSMAP("events map"), CHOICES("choices"), SHUTDOWN("shutdown");

        private String choice;
        private static Map<String, MenuEnum> map = new HashMap<String, MenuEnum>();

        private MenuEnum(String choice)
        {
            this.choice = choice.toLowerCase();
        }

        static
        {
            for(MenuEnum menuEnum : MenuEnum.values())
            {
                map.put(menuEnum.choice, menuEnum);
            }
        }

        public static MenuEnum get(String choice)
        {
            return map.get(choice);
        }
    }

    public static void main(String[] args)
    {
        try
        {
            Socket dataSocket = new Socket("localhost", TollEventServiceDetails.LISTENING_PORT);

            PrintWriter output = new PrintWriter(dataSocket.getOutputStream(), true);

            Scanner input = new Scanner(dataSocket.getInputStream());

            String message = "";

            ObjectMapper objectMapper = new ObjectMapper();

            String response = "";

            message = TollEventServiceDetails.REGISTERED_VEHICLES;

            output.println(message);

            response = input.nextLine();

            StringBuffer newResponse = new StringBuffer(response);
            newResponse.deleteCharAt(0);
            newResponse.deleteCharAt(response.length()-2);

            for(int i=0; i<22; i++)
            {
                newResponse.deleteCharAt(0);
            }
            response = newResponse.toString();

            loadRegistrations(response, objectMapper);
            readTollEvents();

            printChoices();

            while (!message.equals(TollEventServiceDetails.CLOSE))
            {
                List<TollEvent> tollEvents;
                List<String> registrations;
                Map<String, List<TollEvent>> tollEventsAsMap;
                System.out.println("\nEnter choice ('choices' to view list): ");
                response = "";
                String choice = keyboard.nextLine();
                MenuEnum menuEnum = MenuEnum.get(choice);
                Timestamp timestamp;

                switch (menuEnum)
                {
                    case SHUTDOWN:
                        message = TollEventServiceDetails.CLOSE;

                        output.println(message);

                        response = input.nextLine();
                        if(response.equals(TollEventServiceDetails.SESSION_TERMINATED))
                        {
                            System.out.println("Session ended");
                        }
                        break;
                    case HEARTBEAT:
                        message = TollEventServiceDetails.HEARTBEAT;
                        output.println(message);
                        response = input.nextLine();
                        System.out.println(response);
                        break;
                    case BILLING:
                        customerBills.clear();
                        System.out.println("Please enter the name of the customer you want to generate a bill for");
                        String name = keyboard.nextLine();

                        message = TollEventServiceDetails.CUSTOMER_BILL+TollEventServiceDetails.COMMAND_SEPARATOR+name;
                        output.println(message);
                        response = input.nextLine();

                        StringBuffer billsList = new StringBuffer(response);
                        billsList.deleteCharAt(0);
                        billsList.deleteCharAt(response.length()-2);

                        for(int i=0; i<26; i++)
                        {
                            billsList.deleteCharAt(0);
                        }
                        response = billsList.toString();
                        System.out.println(response);
                        loadBills(response, objectMapper);
                        printBills();
                        break;
                    case WRITE:
                        for (Map.Entry<String, List<TollEvent>> entry : tollEventsMap.entrySet())
                        {
                            String registration = entry.getKey();
                            List<TollEvent> tollEventsForRegistration = entry.getValue();
                            if (lookupTable.contains(registration))
                            {
                                for (TollEvent tollEvent : tollEventsForRegistration)
                                {
                                    StringBuilder tollEventmessage = new StringBuilder(TollEventServiceDetails.REGISTER_VALID_EVENT);
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getTollBoothID());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getVehicleReg());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getImageID());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String date = dateFormat.format(tollEvent.getTimestamp());

                                    tollEventmessage.append(date);
                                    message = tollEventmessage.toString();

                                    output.println(message);
                                    response = input.nextLine();
                                    if(response.contains("RegisteredValidEvent"))
                                    {
                                        tollEventsToRemove.add(tollEvent);
                                    }
                                    else
                                    {
                                        System.out.println("Error sending valid toll event to server");
                                    }
                                }
                            }
                            else
                            {
                                for (TollEvent tollEvent : tollEventsForRegistration)
                                {
                                    StringBuffer tollEventmessage = new StringBuffer(TollEventServiceDetails.REGISTER_INVALID_EVENT);
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getTollBoothID());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getVehicleReg());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getImageID());
                                    tollEventmessage.append(TollEventServiceDetails.COMMAND_SEPARATOR);
                                    tollEventmessage.append(tollEvent.getTimestamp());
                                    message = tollEventmessage.toString();

                                    output.println(message);
                                    response = input.nextLine();
                                    if(response.contains("RegisteredInvalidEvent"))
                                    {
                                        tollEventsToRemove.add(tollEvent);
                                    }
                                    else
                                    {
                                        System.out.println("Error sending invalid toll event to server");
                                    }
                                }
                            }
                            readTollEvents();
                        }
                        System.out.println("Found toll events have been sent to the server.");
                        break;
                    case VALID:
                        System.out.println("List of valid registrations: ");
                        for(String registration : lookupTable)
                        {
                            System.out.println(registration);
                        }
                        break;
                    case INVALID:
                        displayInvalidRegistrations();
                        break;
                    case EVENTDETAILS:
                        tollEvents = ITollEventDao.getAllTollEvents();
                        displayTollEvents(tollEvents);
                        break;
                    case REGISTRATIONDETAILS:
                        System.out.println("\nPlease enter the registration that you want to view the toll event details for: ");
                        String registration = keyboard.nextLine();
                        tollEvents = ITollEventDao.getAllTollEventsForRegistration(registration);
                        if(tollEvents.size()<1)
                        {
                            System.out.println("No toll events have been found for the entered registration.");
                        }
                        else
                        {
                            displayTollEvents(tollEvents);
                        }
                        break;
                    case EVENTSSINCE:
                        timestamp = createTimestamp();
                        tollEvents = ITollEventDao.getAllTollEventsSinceSpecifiedDateTime(timestamp);
                        displayTollEvents(tollEvents);
                        keyboard.nextLine();
                        break;
                    case EVENTSBETWEEN:
                        timestamp = createTimestamp();
                        System.out.println("\nFirst time specified. Please specify the second time.");
                        Timestamp timestamp2 = createTimestamp();
                        tollEvents = ITollEventDao.getAllTollEventsBetweenStartFinishDate(timestamp, timestamp2);
                        displayTollEvents(tollEvents);
                        keyboard.nextLine();
                        break;
                    case REGISTRATIONS:
                        registrations = ITollEventDao.getAllRegistrationsForTollAlphabetical();
                        System.out.println("List of registrations that passed through the toll:");
                        for(int i=0; i<registrations.size(); ++i)
                        {
                            System.out.println(registrations.get(i));
                        }
                        break;
                    case EVENTSMAP:
                        tollEventsAsMap = ITollEventDao.tollEventsAsMap();
                        displayTollEventsMap(tollEventsAsMap);
                        break;
                    case CHOICES:
                        printChoices();
                        break;
                    default:
                        System.out.println("Please enter one of the listed choices. Enter 'choices' to view list. ");
                        break;
                }
            }
        }
        catch (InputMismatchException ime)
        {
            System.out.println("Please enter one of the listed choices. Enter 'choices' to view list. ");
        }
        catch (NullPointerException npe)
        {
            System.out.println("Please enter one of the listed choices. Enter 'choices' to view list. ");
        }
        catch (DaoException de)
        {
            System.out.println(de.getMessage());
        }
        catch (UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    public static void loadRegistrations(String json, ObjectMapper mapper)
    {
        try
        {
            Set<String> registrations = mapper.readValue(json, new TypeReference<HashSet<String>>() {});
            for(String registration : registrations)
            {
                lookupTable.add(registration);
            }
            System.out.println("Registrations obtained from server\n");
        }
        catch(JsonMappingException jme)
        {
            System.out.println(jme.getMessage());
        }
        catch(JsonProcessingException jpe)
        {
            System.out.println(jpe.getMessage());
        }
    }

    public static void removeTollEvents()
    {
        for(TollEvent tollEventToRemove : tollEventsToRemove)
        {
            for (Map.Entry<String, List<TollEvent>> entry : tollEventsMap.entrySet())
            {
                List<TollEvent> tollEventsForRegistration = entry.getValue();

                for (TollEvent tollEvent : tollEventsForRegistration)
                {
                    if (tollEvent.equals(tollEventToRemove))
                    {
                        tollEventsForRegistration.remove(tollEvent);
                    }
                }
            }
        }
    }

    public static void loadBills(String json, ObjectMapper mapper)
    {
        try
        {
            List<CustomerBill> bills = mapper.readValue(json, new TypeReference<List<CustomerBill>>() {});
            for(CustomerBill bill : bills)
            {
                customerBills.add(bill);
            }
            System.out.println("Customer bills obtained from server");
        }
        catch(JsonMappingException jme)
        {
            System.out.println(jme.getMessage());
        }
        catch(JsonProcessingException jpe)
        {
            System.out.println(jpe.getMessage());
        }
    }

    public static void printBills()
    {
        for(CustomerBill bill : customerBills)
        {
            System.out.println("Vehicle type: " + bill.getVehicleType());
            System.out.println("Vehicle registration: " + bill.getVehicleRegistration());
            System.out.println("Timestamp: " + bill.getTimestamp());
            System.out.println("Cost: " + bill.getCost());
            System.out.print("\n");
        }
    }

    private static void readTollEvents()
    {
        try(Scanner scanner = new Scanner(new FileReader("Toll-Events.csv")))
        {
            scanner.useDelimiter(";|\\n");
            String tollBoothID;
            String vehicleReg;
            long imageID;
            Timestamp timestamp;

            while(scanner.hasNext())
            {
                tollBoothID = scanner.next();
                vehicleReg = scanner.next();

                boolean isValid = isValidRegistration(vehicleReg);
                if(isValid == true)
                {
                    imageID = Long.parseLong(scanner.next());
                    //Uses DateUtil class to convert a String to a timestamp
                    //https://stackoverflow.com/questions/18915075/java-convert-string-to-timestamp
                    timestamp = new Timestamp(DateUtil.provideDateFormat().parse(scanner.next()).getTime());

                    TollEvent tollEvent = new TollEvent(tollBoothID, vehicleReg, imageID, timestamp);
                    List<TollEvent> tollEvents;
                    if (tollEventsMap.containsKey(vehicleReg))
                    {
                        tollEvents = tollEventsMap.get(vehicleReg);
                        tollEvents.add(tollEvent);
                    }
                    else
                    {
                        tollEvents = new ArrayList<TollEvent>();
                        tollEvents.add(tollEvent);
                        tollEventsMap.put(vehicleReg, tollEvents);
                    }
                }
                else
                {
                    scanner.nextLine();
                }
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /*private static void loadRegistrations()
    {
        try(Scanner scanner = new Scanner(new FileReader("Vehicles.csv")))
        {
            String registration;
            while(scanner.hasNextLine())
            {
               registration = scanner.nextLine();
               lookupTable.add(registration);
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }*/

    private static boolean isValidRegistration(String registration)
    {
        if(lookupTable.contains(registration))
        {
            return true;
        }

        invalidRegistrations.add(registration);
        return false;
    }

    private static void displayInvalidRegistrations()
    {
        if(invalidRegistrations.size() > 0)
        {
            System.out.println("List of invalid registrations:");

            for (String registration : invalidRegistrations)
            {
                System.out.println(registration);
            }
        }
        else
        {
            System.out.println("No invalid registrations found");
        }
    }

    private static void displayTollEventsMap(Map<String, List<TollEvent>> tollEventsMap)
    {
        for (Map.Entry<String, List<TollEvent>> entry : tollEventsMap.entrySet())
        {
            String registration = entry.getKey();
            List<TollEvent> tollEvents = entry.getValue();
            System.out.println("\nToll events for registration " + registration + ":");
            for(TollEvent tollEvent : tollEvents)
            {
                System.out.println("\n" + "Image Id: " + tollEvent.getImageID() + "\n" + "Timestamp: " + tollEvent.getTimestamp());
            }
        }
    }

    private static void displayTollEvents(List<TollEvent> tollEvents)
    {
        //System.out.print("\n");
        for (TollEvent tollEvent: tollEvents)
        {
            System.out.print("\n");
            System.out.println("Toll booth ID: " + tollEvent.getTollBoothID() + "\n" + "Registration: " + tollEvent.getVehicleReg() + "\n" + "Image Id: " + tollEvent.getImageID() + "\n" + "Timestamp: " + tollEvent.getTimestamp());
        }
    }

    private static Timestamp createTimestamp()
    {
        int year = getYear();
        int month = getMonth();
        int day = getDay();
        int hour = getHour();
        int minute = getMinute();
        int second = getSecond();

        String date = year + "-" + month + "-" + day + 'T' + hour + ":" + minute + ":" + second + ".0" + 'Z';

        Timestamp timestamp = null;
        try
        {
            timestamp = new Timestamp(DateUtil.provideDateFormat().parse(date).getTime());
        }
        catch (ParseException pe)
        {
            System.out.println(pe.getMessage());
        }
        return timestamp;
    }

    private static int getYear()
    {
        int year = 2000;
        boolean finishedYear = false;
        while(!finishedYear)
        {
            try
            {
                System.out.println("Enter the year:");
                year = keyboard.nextInt();
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number for the year");
                keyboard.nextLine();
                continue;
            }
            finishedYear = true;
        }
        return year;
    }

    private static int getMonth()
    {
        int month = 1;
        boolean finishedMonth = false;
        while(!finishedMonth)
        {
            try
            {
                System.out.println("Enter the month:");
                month = keyboard.nextInt();

                if(month < 1 || month > 12)
                {
                    System.out.println("\nPlease enter a month between 1 and 12");
                    keyboard.nextLine();
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number between 1 and 12");
                keyboard.nextLine();
                continue;
            }
            finishedMonth = true;
        }
        return month;
    }

    private static int getDay()
    {
        int day = 1;
        boolean finishedDay = false;
        while(!finishedDay)
        {
            try
            {
                System.out.println("Enter the day:");
                day = keyboard.nextInt();

                if(day < 1 || day > 31)
                {
                    System.out.println("\nPlease enter a day between 1 and 31");
                    keyboard.nextLine();
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number between 1 and 31");
                keyboard.nextLine();
                continue;
            }
            finishedDay = true;
        }
        return day;
    }

    private static int getHour()
    {
        int hour = 1;
        boolean finishedHours = false;
        while(!finishedHours)
        {
            try
            {
                System.out.println("Enter the hour:");
                hour = keyboard.nextInt();

                if(hour < 0 || hour > 23)
                {
                    System.out.println("\nPlease enter an hour between 0 and 23");
                    keyboard.nextLine();
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number between 0 and 23");
                keyboard.nextLine();
                continue;
            }
            finishedHours = true;
        }
        return hour;
    }

    private static int getMinute()
    {
        int minute = 1;
        boolean finishedMinutes = false;
        while(!finishedMinutes)
        {
            try
            {
                System.out.println("Enter the minute:");
                minute = keyboard.nextInt();

                if(minute < 0 || minute > 59)
                {
                    System.out.println("\nPlease enter a minute between 0 and 59");
                    keyboard.nextLine();
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number between 0 and 59");
                keyboard.nextLine();
                continue;
            }
            finishedMinutes = true;
        }
        return minute;
    }

    private static int getSecond()
    {
        int second = 1;
        boolean finishedSeconds = false;
        while(!finishedSeconds)
        {
            try
            {
                System.out.println("Enter the second:");
                second = keyboard.nextInt();

                if(second < 0 || second > 59)
                {
                    System.out.println("\nPlease enter a second between 0 and 59");
                    keyboard.nextLine();
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter a number between 0 and 59");
                keyboard.nextLine();
                continue;
            }
            finishedSeconds = true;
        }
        return second;
    }

    private static void printChoices()
    {
        System.out.println("Available choices: ");
        System.out.println("Heartbeat - send a heartbeat message to the server");
        System.out.println("Write - send found toll events to the server and then to the database");
        System.out.println("Billing - generate a list of bills for a customer");
        System.out.println("Valid - view a list of valid registrations in the lookup table");
        System.out.println("Invalid - view invalid registrations");
        System.out.println("Event details - get all toll event details");
        System.out.println("Registration details - get toll Event details for a registration");
        System.out.println("Events since - get all toll event details that happened since a specified date-time");
        System.out.println("Events between - get all toll event details that happened between a start date-time and a finish date-time");
        System.out.println("Registrations - get all Registrations that passed through the toll");
        System.out.println("Events map - get all toll event details returned as a map of registration and list of toll events");
        System.out.println("Choices - print list of available choices");
        System.out.println("Shutdown - shutdown the program");
    }
}
