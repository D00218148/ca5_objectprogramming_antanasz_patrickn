/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.patricknugent;
//DateUtil class used to convert String to Timestamp in MainApp
//https://stackoverflow.com/questions/18915075/java-convert-string-to-timestamp
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public interface DateUtil
{

    String ISO_DATE_FORMAT_ZERO_OFFSET = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String UTC_TIMEZONE_NAME = "UTC";

    static SimpleDateFormat provideDateFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ISO_DATE_FORMAT_ZERO_OFFSET);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TIMEZONE_NAME));
        return simpleDateFormat;
    }

    static SimpleDateFormat provideUserTimestampDateFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ISO_DATE_FORMAT_ZERO_OFFSET);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TIMEZONE_NAME));
        return simpleDateFormat;
    }
}
