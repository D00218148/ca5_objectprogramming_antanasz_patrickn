/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.patricknugent;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.dkit.gd2.antanaszalisauskas.Server.Command;
import com.dkit.gd2.antanaszalisauskas.Server.CommandFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TollEventClientThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public TollEventClientThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(dataSocket.getInputStream());
            output = new PrintWriter(dataSocket.getOutputStream(), true);
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(TollEventServiceDetails.CLOSE))
            {
                response = null;

                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                String[] components = incomingMessage.split(TollEventServiceDetails.COMMAND_SEPARATOR);

                Command c = CommandFactory.createCommand(components[0]);

                if(c != null)
                {
                    response = c.createResponse(components);

                    if(response != null)
                    {
                        output.println(response);
                    }
                }
            }
        }
        catch(NoSuchElementException nse)
        {
            System.out.println(nse.getMessage());
        }
        catch(DaoException de)
        {
            System.out.println(de.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("\n Closing connection with client #" + number + "...");
                dataSocket.close();
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }
}
