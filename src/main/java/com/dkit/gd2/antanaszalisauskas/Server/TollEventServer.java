/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.dkit.gd2.patricknugent.TollEventClientThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TollEventServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        ThreadGroup group = null;

        try
        {
            listeningSocket = new ServerSocket(TollEventServiceDetails.LISTENING_PORT);

            group = new ThreadGroup("Client Threads");
            group.setMaxPriority(Thread.currentThread().getPriority()-1);

            boolean continueRunning = true;
            while(continueRunning)
            {
                dataSocket = listeningSocket.accept();
                TollEventClientThread newClient = new TollEventClientThread(group, dataSocket.getInetAddress() + "", dataSocket, group.activeCount());
                newClient.start();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Error closing listening socket");
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
    }


}
