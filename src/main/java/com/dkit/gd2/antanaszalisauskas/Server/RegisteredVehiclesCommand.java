/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.dkit.gd2.patricknugent.MySqlDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.*;
import java.util.*;

public class RegisteredVehiclesCommand extends MySqlDAO implements Command
{
    @Override
    public String createResponse(String[] components) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> vehicles = new HashSet<>();
        String jsonString = "";
        Map<String, Set> stringToConvert = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            con = this.getConnection();
            String query = "select * from vehicle";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String vehicleReg = rs.getString("vehicle_registration");
                vehicles.add(vehicleReg);
            }

            stringToConvert.put(TollEventServiceDetails.REGISTERED_VEHICLES, vehicles);
            jsonString = mapper.writeValueAsString(stringToConvert);
        }
        catch(SQLException e)
        {
            throw new DaoException("Error retrieving registered vehicles " + e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println("Error creating json String");
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("RegisteredVehicles failed to close or free connection " + e.getMessage());
            }
        }
        return jsonString;
    }
}
