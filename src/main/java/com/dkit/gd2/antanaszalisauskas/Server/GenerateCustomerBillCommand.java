/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.antanaszalisauskas.CustomerBill;
import com.dkit.gd2.patricknugent.MySqlDAO;
import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.*;
import java.util.*;

public class GenerateCustomerBillCommand extends MySqlDAO implements Command
{
    @Override
    public String createResponse(String[] components) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String jsonString = "";
        List<CustomerBill> bill = new ArrayList<>();
        Map<String, List> stringToConvert = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            con = this.getConnection();
            String query = "select vehicle_type, vehicle_registration, timestamp, cost from toll_event natural join customer_vehicle natural join customer natural join vehicle natural join vehicle_type_cost where customer_name = \"" + components[1] + "\" and timestamp BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String vehicleType = rs.getString("vehicle_type");
                String vehicleReg = rs.getString("vehicle_registration");
                Timestamp timestamp = rs.getTimestamp("timestamp");
                int cost = rs.getInt("cost");
                bill.add(new CustomerBill(vehicleType, vehicleReg, timestamp, cost));
            }

            stringToConvert.put(TollEventServiceDetails.CUSTOMER_BILL, bill);
            jsonString = mapper.writeValueAsString(stringToConvert);
        }
        catch(SQLException e)
        {
            throw new DaoException("Error retrieving registered vehicles " + e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println("Error creating json String");
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("RegisteredVehicles failed to close or free connection " + e.getMessage());
            }
        }
        return jsonString;
    }
}
