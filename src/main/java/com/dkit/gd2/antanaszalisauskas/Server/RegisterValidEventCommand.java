/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.Server.Command;
import com.dkit.gd2.patricknugent.MySqlDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class RegisterValidEventCommand extends MySqlDAO implements Command
{
    @Override
    public String createResponse(String[] components) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String jsonString = "";
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> stringToConvert = new HashMap<>();

        try
        {
            con = this.getConnection();
            //String query = "insert into toll_event values('" + components[1] + "','" + components[2] + "'," + components[3] + ",'" + components[4] + "')";
            String query = "insert into toll_event(toll_booth_id, vehicle_registration, image_id, timestamp) values('" + components[1] + "','" + components[2] + "'," + components[3] + ",'" + components[4] + "')";
            ps = con.prepareStatement(query);
            ps.executeUpdate();

            stringToConvert.put("PacketType", "RegisteredValidEvent");
            jsonString = mapper.writeValueAsString(stringToConvert);
        }
        catch(SQLException e)
        {
            throw new DaoException("Error inserting valid toll event " + e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println("Error creating json String");
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("RegisterValidEvent failed to close or free connection " + e.getMessage());
            }
        }
        return jsonString;
    }
}
