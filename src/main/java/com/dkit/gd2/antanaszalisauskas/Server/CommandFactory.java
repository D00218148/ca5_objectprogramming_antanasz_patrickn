/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;

public class CommandFactory
{
    public static Command createCommand(String command)
    {
        Command c = null;

        if(command.equals(TollEventServiceDetails.REGISTERED_VEHICLES))
        {
            c = new RegisteredVehiclesCommand();
        }
        else if(command.equals(TollEventServiceDetails.HEARTBEAT))
        {
            c = new HeartbeatCommand();
        }
        else if(command.equals(TollEventServiceDetails.REGISTER_VALID_EVENT))
        {
            c = new RegisterValidEventCommand();
        }
        else if(command.equals(TollEventServiceDetails.REGISTER_INVALID_EVENT))
        {
            c = new RegisterInvalidEventCommand();
        }
        else if(command.equals(TollEventServiceDetails.CUSTOMER_BILL))
        {
            c = new GenerateCustomerBillCommand();
        }

        return c;
    }


}
