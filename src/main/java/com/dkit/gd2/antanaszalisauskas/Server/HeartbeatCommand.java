/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class HeartbeatCommand implements Command
{
    @Override
    public String createResponse(String[] components) throws DaoException
    {
        Map<String, String> stringToConvert = new HashMap<>();
        String jsonString = "";
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            stringToConvert.put(TollEventServiceDetails.HEARTBEAT, "Heartbeat Response");
            jsonString = mapper.writeValueAsString(stringToConvert);
        }
        catch(JsonProcessingException e)
        {
            System.out.println("Error creating json string");
            System.out.println(e.getMessage());
        }

        return jsonString;
    }
}
