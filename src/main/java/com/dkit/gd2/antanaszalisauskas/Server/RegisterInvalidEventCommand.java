/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas.Server;

import com.dkit.gd2.antanaszalisauskas.Core.TollEventServiceDetails;
import com.dkit.gd2.antanaszalisauskas.TollEvent;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RegisterInvalidEventCommand implements Command
{
    @Override
    public String createResponse(String[] components)
    {
        List<TollEvent> invalidEvents = new ArrayList<>();
        TollEvent event = new TollEvent(components[1], components[2], Long.parseLong(components[3]), Timestamp.valueOf(components[4]));
        invalidEvents.add(event);
        String json = "";
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> stringToConvert = new HashMap<>();
            stringToConvert.put(TollEventServiceDetails.REGISTER_INVALID_EVENT, "RegisteredInvalidEvent");
            json = mapper.writeValueAsString(stringToConvert);
        }
        catch(JsonProcessingException e)
        {
            System.out.println("Error creating json string");
            System.out.println(e.getMessage());
        }
        return json;
    }
}
