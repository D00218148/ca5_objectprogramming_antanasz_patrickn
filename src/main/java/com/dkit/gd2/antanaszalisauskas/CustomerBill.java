/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas;

import java.sql.Timestamp;

public class CustomerBill
{
    private String vehicleType;
    private String vehicleRegistration;
    private Timestamp timestamp;
    private int cost;

    public CustomerBill(String vehicleType, String vehicleRegistration, Timestamp timestamp, int cost)
    {
        this.vehicleType = vehicleType;
        this.vehicleRegistration = vehicleRegistration;
        this.timestamp = timestamp;
        this.cost = cost;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getVehicleRegistration() {
        return vehicleRegistration;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public int getCost() {
        return cost;
    }
}
