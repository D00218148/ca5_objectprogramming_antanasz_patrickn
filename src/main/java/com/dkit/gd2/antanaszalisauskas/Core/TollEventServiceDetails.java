/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */

package com.dkit.gd2.antanaszalisauskas.Core;

public class TollEventServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    public static final String COMMAND_SEPARATOR = "%%";

    public static final String CLOSE = "QUIT";
    public static final String REGISTERED_VEHICLES = "REGISTERED_VEHICLES";
    public static final String HEARTBEAT = "HEARTBEAT";
    public static final String REGISTER_VALID_EVENT = "REGISTERED_VALID_TOLL_EVENT";
    public static final String REGISTER_INVALID_EVENT = "REGISTERED_INVALID_TOLL_EVENT";
    public static final String CUSTOMER_BILL = "GENERATED_CUSTOMER_BILL";

    public static final String UNRECOGNISED = "UNKNOWN_COMMAND";
    public static final String SESSION_TERMINATED = "GOODBYE";
}
