/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas;

import com.dkit.gd2.Exceptions.DaoException;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ITollEventDaoInterface
{
    public List<TollEvent> getAllTollEvents() throws DaoException;
    public List<TollEvent> getAllTollEventsForRegistration(String registration) throws DaoException;
    public List<TollEvent> getAllTollEventsSinceSpecifiedDateTime(Timestamp specifiedDate) throws DaoException;
    public List<TollEvent> getAllTollEventsBetweenStartFinishDate(Timestamp start, Timestamp finish) throws DaoException;
    public List<String> getAllRegistrationsForTollAlphabetical() throws DaoException;
    public Map<String, List<TollEvent>> tollEventsAsMap() throws DaoException;
    public Set<String> getAllRegistrations() throws DaoException;
    public void writeTollEventsToDatabase(Map<String, List<TollEvent>> tollEvents) throws DaoException;
    public void writeVehiclesToDatabase(ArrayList<String> registrations) throws DaoException;
    public void cleanDatabase() throws DaoException;
}
