/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.patricknugent.DateUtil;
import org.junit.After;
import org.junit.Before;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

import static org.junit.Assert.*;

public class MySqlTollEventDAOTest {

    private MySqlTollEventDAO tollEventDao;
    private ArrayList<TollEvent> testEvents;
    private List<TollEvent> tollEvents;
    private List<String> registrations;
    private Map<String, List<TollEvent>> tollEventsMap;

    @Before
    public void setUp() throws Exception {
        /*
        tollEventDao = new MySqlTollEventDAO();
        testEvents = new ArrayList<>();
        tollEvents = new ArrayList<>();
        registrations = new ArrayList<>();
        tollEventsMap = new HashMap<>();
         */
    }

    @After
    public void after() throws Exception {
        /*
        try
        {
            tollEventDao.cleanDatabase();
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
         */
    }

    /*
    @org.junit.Test
    public void getAllTollEventsTest() {
        try
        {
            TollEvent t1 = new TollEvent("191LH1111", 30402, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t2 = new TollEvent("191LH1111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t3 = new TollEvent("191LH2111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));

            testEvents.add(t1);
            testEvents.add(t2);
            tollEventsMap.put(t1.getVehicleReg(), new ArrayList<>(testEvents));

            testEvents.clear();
            testEvents.add(t3);
            tollEventsMap.put(t3.getVehicleReg(), new ArrayList<>(testEvents));

            tollEventDao.writeTollEventsToDatabase(tollEventsMap);

            tollEvents = tollEventDao.getAllTollEvents();

            assertEquals("191LH1111", tollEvents.get(0).getVehicleReg());
            assertEquals(30402, tollEvents.get(0).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(0).getTimestamp());

            assertEquals("191LH1111", tollEvents.get(1).getVehicleReg());
            assertEquals(30403, tollEvents.get(1).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(1).getTimestamp());

            assertEquals("191LH2111", tollEvents.get(2).getVehicleReg());
            assertEquals(30403, tollEvents.get(2).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(2).getTimestamp());
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @org.junit.Test
    public void getAllTollEventsForRegistrationTest() {
        try
        {
            TollEvent t1 = new TollEvent("191LH1111", 30402, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t2 = new TollEvent("191LH1111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t3 = new TollEvent("191LH2111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));

            testEvents.add(t1);
            testEvents.add(t2);
            tollEventsMap.put(t1.getVehicleReg(), new ArrayList<>(testEvents));

            testEvents.clear();
            testEvents.add(t3);
            tollEventsMap.put(t3.getVehicleReg(), new ArrayList<>(testEvents));

            tollEventDao.writeTollEventsToDatabase(tollEventsMap);

            tollEvents = tollEventDao.getAllTollEventsForRegistration("191LH1111");

            assertEquals("191LH1111", tollEvents.get(0).getVehicleReg());
            assertEquals(30402, tollEvents.get(0).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(0).getTimestamp());

            assertEquals("191LH1111", tollEvents.get(1).getVehicleReg());
            assertEquals(30403, tollEvents.get(1).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(1).getTimestamp());

        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @org.junit.Test
    public void getAllTollEventsSinceSpecifiedDateTimeTest() {
        try
        {
            TollEvent t1 = new TollEvent("191LH1111", 30402, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t2 = new TollEvent("191LH1111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t3 = new TollEvent("191LH2111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));

            testEvents.add(t1);
            testEvents.add(t2);
            tollEventsMap.put(t1.getVehicleReg(), new ArrayList<>(testEvents));

            testEvents.clear();
            testEvents.add(t3);
            tollEventsMap.put(t3.getVehicleReg(), new ArrayList<>(testEvents));

            tollEventDao.writeTollEventsToDatabase(tollEventsMap);

            tollEvents = tollEventDao.getAllTollEventsSinceSpecifiedDateTime(new Timestamp(DateUtil.provideDateFormat().parse("2021-01-14T10:15:30.0Z").getTime()));

            assertEquals("191LH1111", tollEvents.get(0).getVehicleReg());
            assertEquals(30402, tollEvents.get(0).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(0).getTimestamp());

            assertEquals("191LH1111", tollEvents.get(1).getVehicleReg());
            assertEquals(30403, tollEvents.get(1).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(1).getTimestamp());

            assertEquals("191LH2111", tollEvents.get(2).getVehicleReg());
            assertEquals(30403, tollEvents.get(2).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(2).getTimestamp());
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @org.junit.Test
    public void getAllTollEventsBetweenStartFinishDateTest() {
        try
        {
            TollEvent t1 = new TollEvent("191LH1111", 30402, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t2 = new TollEvent("191LH1111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-03-14T10:15:30.0Z").getTime()));
            TollEvent t3 = new TollEvent("191LH2111", 30404, new Timestamp(DateUtil.provideDateFormat().parse("2020-04-14T10:15:30.0Z").getTime()));

            testEvents.add(t1);
            testEvents.add(t2);
            tollEventsMap.put(t1.getVehicleReg(), new ArrayList<>(testEvents));

            testEvents.clear();
            testEvents.add(t3);
            tollEventsMap.put(t3.getVehicleReg(), new ArrayList<>(testEvents));

            tollEventDao.writeTollEventsToDatabase(tollEventsMap);

            tollEvents = tollEventDao.getAllTollEventsBetweenStartFinishDate(new Timestamp(DateUtil.provideDateFormat().parse("2020-01-14T10:15:30.0Z").getTime()), new Timestamp(DateUtil.provideDateFormat().parse("2020-03-14T10:15:30.0Z").getTime()));

            assertEquals("191LH1111", tollEvents.get(0).getVehicleReg());
            assertEquals(30402, tollEvents.get(0).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()), tollEvents.get(0).getTimestamp());

            assertEquals("191LH1111", tollEvents.get(1).getVehicleReg());
            assertEquals(30403, tollEvents.get(1).getImageID());
            assertEquals(new Timestamp(DateUtil.provideDateFormat().parse("2020-03-14T10:15:30.0Z").getTime()), tollEvents.get(1).getTimestamp());
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @org.junit.Test
    public void getAllRegistrationsForTollAlphabeticalTest() {
        try
        {
            ArrayList<String> testRegistrations = new ArrayList<>();
            testRegistrations.add("161C3457");
            testRegistrations.add("151DL200");
            testRegistrations.add("152DL345");
            testRegistrations.add("151DL200"); //duplicate entry
            testRegistrations.add("181MH3456");

            tollEventDao.writeVehiclesToDatabase(testRegistrations);

            registrations = tollEventDao.getAllRegistrationsForTollAlphabetical();

            assertEquals("151DL200", registrations.get(0));
            assertEquals("152DL345", registrations.get(1));
            assertEquals("161C3457", registrations.get(2));
            assertEquals("181MH3456", registrations.get(3));
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @org.junit.Test
    public void tollEventsAsMapTest() {
        try
        {
            Map<String, List<TollEvent>> tollEvents;

            TollEvent t1 = new TollEvent("191LH1111", 30402, new Timestamp(DateUtil.provideDateFormat().parse("2020-02-14T10:15:30.0Z").getTime()));
            TollEvent t2 = new TollEvent("191LH1111", 30403, new Timestamp(DateUtil.provideDateFormat().parse("2020-03-14T10:15:30.0Z").getTime()));
            TollEvent t3 = new TollEvent("191LH2111", 30404, new Timestamp(DateUtil.provideDateFormat().parse("2020-04-14T10:15:30.0Z").getTime()));

            testEvents.add(t1);
            testEvents.add(t2);
            tollEventsMap.put(t1.getVehicleReg(), new ArrayList<>(testEvents));

            testEvents.clear();
            testEvents.add(t3);
            tollEventsMap.put(t3.getVehicleReg(), new ArrayList<>(testEvents));

            tollEventDao.writeTollEventsToDatabase(tollEventsMap);

            tollEvents = tollEventDao.tollEventsAsMap();

            //check if key exists
            assertTrue(tollEvents.containsKey("191LH1111"));
            assertTrue(tollEvents.containsKey("191LH2111"));

            assertEquals(t1, tollEvents.get("191LH1111").get(0));
            assertEquals(t2, tollEvents.get("191LH1111").get(1));
            assertEquals(t3, tollEvents.get("191LH2111").get(0));
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ParseException e)
        {
            System.out.println(e.getMessage());
        }
    }

     */
}