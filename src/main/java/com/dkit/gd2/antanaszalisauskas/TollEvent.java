/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

public class TollEvent
{
    private String tollBoothID;
    private String vehicleReg;
    private long imageID;
    private Timestamp timestamp;

    public TollEvent(String tollBoothID, String vehicleReg, long imageID, Timestamp timestamp) {
        this.tollBoothID = tollBoothID;
        this.vehicleReg = vehicleReg;
        this.imageID = imageID;
        this.timestamp = timestamp;
    }

    public String getTollBoothID() {
        return tollBoothID;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public long getImageID() {
        return imageID;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TollEvent)) return false;
        TollEvent tollEvent = (TollEvent) o;
        return  imageID == tollEvent.imageID &&
                Objects.equals(tollBoothID, tollEvent.tollBoothID) &&
                Objects.equals(vehicleReg, tollEvent.vehicleReg) &&
                Objects.equals(timestamp, tollEvent.timestamp);
    }

    @Override
    public int hashCode() {
        return vehicleReg.hashCode() + 57;
    }

    @Override
    public String toString() {
        return "TollEvent{" +
                "tollBoothID='" + tollBoothID + '\'' +
                ", vehicleReg='" + vehicleReg +
                ", imageID=" + imageID +
                ", timestamp=" + timestamp +
                '}';
    }
}
