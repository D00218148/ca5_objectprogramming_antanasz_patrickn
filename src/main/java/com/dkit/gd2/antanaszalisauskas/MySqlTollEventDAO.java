/*
 * Name: Patrick Nugent
 * Student ID: D00218208
 *
 * Name: Antanas Zalisauskas
 * Student ID: D00218148
 */
package com.dkit.gd2.antanaszalisauskas;

import com.dkit.gd2.Exceptions.DaoException;
import com.dkit.gd2.patricknugent.MySqlDAO;

import java.sql.*;
import java.sql.Date;
import java.time.Instant;
import java.util.*;

public class MySqlTollEventDAO extends MySqlDAO implements ITollEventDaoInterface
{
    @Override
    public List<TollEvent> getAllTollEvents() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();
            String query = "select * from toll_event";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String tollBoothId = rs.getString("toll_booth_id");
                String vehicleReg = rs.getString("vehicle_registration");
                long imageID = rs.getLong("image_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");

                TollEvent t = new TollEvent(tollBoothId, vehicleReg, imageID, timestamp);
                tollEvents.add(t);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEvents() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public List<TollEvent> getAllTollEventsForRegistration(String registration) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();
            String query = "select * from toll_event WHERE vehicle_registration = '" + registration + "'";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String tollBoothID = rs.getString("event_id");
                String vehicleReg = rs.getString("vehicle_registration");
                long imageID = rs.getLong("image_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");

                TollEvent t = new TollEvent(tollBoothID, vehicleReg, imageID, timestamp);
                tollEvents.add(t);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEventsForRegistration() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEventsForRegistration() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public List<TollEvent> getAllTollEventsSinceSpecifiedDateTime(Timestamp specifiedDate) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();
            String query = "SELECT * FROM toll_events WHERE timestamp < '" + specifiedDate + "'";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String tollBoothID = rs.getString("event_id");
                String vehicleReg = rs.getString("vehicle_registration");
                long imageID = rs.getLong("image_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");

                TollEvent t = new TollEvent(tollBoothID, vehicleReg, imageID, timestamp);
                tollEvents.add(t);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEventsSinceSpecifiedDateTime() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEventsSinceSpecifiedDateTime() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public List<TollEvent> getAllTollEventsBetweenStartFinishDate(Timestamp start, Timestamp finish) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();
            //code to output values between dates:
            //https://stackoverflow.com/questions/37879517/how-to-display-the-dates-between-from-date-and-to-date-column-in-sql-server
            String query = "SELECT * FROM toll_events WHERE timestamp >= '" + start + "' AND timestamp <= '" + finish + "'";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String tollBoothID = rs.getString("event_id");
                String vehicleReg = rs.getString("vehicle_registration");
                long imageID = rs.getLong("image_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");

                TollEvent t = new TollEvent(tollBoothID, vehicleReg, imageID, timestamp);
                tollEvents.add(t);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEventsBetweenStartFinishDate() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEventsBetweenStartFinishDate() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public List<String> getAllRegistrationsForTollAlphabetical() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();
            String query = "SELECT DISTINCT registration FROM vehicles ORDER BY registration";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String vehicleReg = rs.getString("registration");

                tollEvents.add(vehicleReg);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllRegistrationsForTollAlphabetical() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllRegistrationsForTollAlphabetical() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public Map<String, List<TollEvent>> tollEventsAsMap() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> events = new ArrayList<>();
        List<TollEvent> mapList = new ArrayList<>(); // list which will be put into map for each reg key
        Map<String, List<TollEvent>> tollEvents = new HashMap<>();

        try
        {
            con = this.getConnection();
            String query = "SELECT * FROM toll_events ORDER BY vehicle_registration";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String tollBoothID = rs.getString("event_id");
                String vehicleReg = rs.getString("vehicle_registration");
                long imageID = rs.getLong("image_id");
                Timestamp timestamp = rs.getTimestamp("timestamp");

                TollEvent t = new TollEvent(tollBoothID, vehicleReg, imageID, timestamp);


                if(tollEvents.size() == 0) //checks if the map is empty
                {
                    events.add(t);
                    mapList = new ArrayList<>(events);
                    tollEvents.put(t.getVehicleReg(), mapList);
                }

                //checks if the registration key already exists. If it doesn't
                //it clears the list of events previously added and puts in new ones
                //to do with the new registration
                else if(!tollEvents.containsKey(vehicleReg))
                {
                    events.clear();
                    events.add(t);
                    mapList = new ArrayList<>(events);
                    tollEvents.put(t.getVehicleReg(), mapList);
                }
                else if(events.contains(t))
                {

                }
                else
                {
                    mapList.add(t);
                    tollEvents.put(t.getVehicleReg(), mapList);
                }
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("TollEventsAsMap() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("TollEventsAsMap() " + e.getMessage());
            }
        }

        return tollEvents;
    }

    @Override
    public Set<String> getAllRegistrations() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> registrations = new HashSet<>();

        try
        {
            con = this.getConnection();
            String query = "SELECT vehicle_registration FROM toll_events";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String vehicleReg = rs.getString("vehicle_registration");

                registrations.add(vehicleReg);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllRegistrations() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllRegistrations() " + e.getMessage());
            }
        }

        return registrations;
    }

    @Override
    public void writeTollEventsToDatabase(Map<String, List<TollEvent>> tollEvents) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            con = this.getConnection();


            for(String key: tollEvents.keySet())
            {
                for(int i = 0; i < tollEvents.get(key).size(); i++)
                {
                    String vehicleReg = tollEvents.get(key).get(i).getVehicleReg();
                    long imageID = tollEvents.get(key).get(i).getImageID();
                    Timestamp timestamp = tollEvents.get(key).get(i).getTimestamp();

                    String query = "INSERT INTO toll_event VALUES ('" + vehicleReg + "', " + imageID + ", '" + timestamp + "')";
                    ps = con.prepareStatement(query);
                    ps.executeUpdate();
                }
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("writeTollEventsToDatabase() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("writeTollEventsToDatabase() " + e.getMessage());
            }
        }
    }

    @Override
    public void writeVehiclesToDatabase(ArrayList<String> registrations) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            con = this.getConnection();
            for(String registration: registrations)
            {
                String vehicleReg = registration;

                String query = "INSERT INTO vehicle VALUES ('" + vehicleReg + "')";
                ps = con.prepareStatement(query);
                ps.executeUpdate();
            }

        }
        catch(SQLException e)
        {
            throw new DaoException("writeVehiclesToDatabase() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("writeVehiclesToDatabase() " + e.getMessage());
            }
        }
    }

    //THIS METHOD IS USED FOR TESTING PURPOSES ONLY
    //TO FLUSH DATABASE OF VALUES BEFORE EACH TEST
    @Override
    public void cleanDatabase() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            con = this.getConnection();
            String query = "DELETE FROM toll_event";
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            throw new DaoException("cleanDatabase() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("cleanDatabase() " + e.getMessage());
            }
        }
    }
}
